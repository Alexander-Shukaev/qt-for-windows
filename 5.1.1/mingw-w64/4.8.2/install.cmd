:: Preamble {{{
:: =============================================================================
::        File: install.cmd
:: -----------------------------------------------------------------------------
::     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
:: -----------------------------------------------------------------------------
:: Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
:: -----------------------------------------------------------------------------
::  Copyrights: (C) 2013, Alexander Shukaev <http://Alexander.Shukaev.name>
:: -----------------------------------------------------------------------------
::     License: This program is free software: you can redistribute it and/or
::              modify it under the terms of the GNU General Public License as
::              published by the Free Software Foundation, either version 3 of
::              the License, or (at your option) any later version.
::
::              This program is distributed in the hope that it will be useful,
::              but WITHOUT ANY WARRANTY; without even the implied warranty of
::              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
::              General Public License for more details.
::
::              You should have received a copy of the GNU General Public
::              License along with this program. If not, see
::              <http://www.gnu.org/licenses/>.
:: =============================================================================
:: }}} Preamble

@echo off

setlocal EnableExtensions

set PATCHES_DIR=%~dp0patches

set Qt_SOURCE_DIR=%1
set Qt_BUILD_DIR=%Qt_SOURCE_DIR%\.build
set Qt_INSTALL_DIR=%2

cd %Qt_SOURCE_DIR%

patch.exe --binary --forward -p0 < "%PATCHES_DIR%\qtbase\mkspecs\win32-g++\qmake.conf.patch"
patch.exe --binary --forward -p0 < "%PATCHES_DIR%\qtbase\qmake\Makefile.unix.patch"
patch.exe --binary --forward -p0 < "%PATCHES_DIR%\qtbase\src\corelib\io\qfilesystemengine_win.cpp.patch"
patch.exe --binary --forward -p0 < "%PATCHES_DIR%\qtbase\src\plugins\platforms\windows\qwindowsdialoghelpers.cpp.patch"
patch.exe --binary --forward -p0 < "%PATCHES_DIR%\qtbase\src\plugins\platforms\windows\qwindowstheme.cpp.patch"

md %Qt_BUILD_DIR%
cd %Qt_BUILD_DIR%

call %Qt_SOURCE_DIR%\configure.bat -opensource                                 ^
                                   -confirm-license                            ^
                                   -platform win32-g++                         ^
                                   -release                                    ^
                                   -skip webkit                                ^
                                   -opengl desktop                             ^
                                   -nomake examples                            ^
                                   -nomake tests                               ^
                                   -prefix %Qt_INSTALL_DIR%

set PATH=%PATH%;%Qt_BUILD_DIR%\qtbase\lib

mingw32-make.exe -j

md %Qt_INSTALL_DIR%

mingw32-make.exe -j install

endlocal

:: Modeline {{{
:: =============================================================================
:: vim:ft=dosbatch:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
:: =============================================================================
:: }}} Modeline
